Project Description :

This module will fetch all  1tick  market data from Finance exchange and store it in the database. Also, all the 1 tick data will be aggregated to 5, 10, 15, 30 and 60 minutes Data and user will be able to plot candlestick graph based on the market data.

 The project structure is

 app
    db_handler
    email_service
    reporting
    static
    templates
    utility
    views

  1. db_handler :  This module handles all the database related task.
  2. email_service : Module to handle the emailing service and SES is used for sending the email.
  3. Reporting : Generate the market report and pass the data to email service.
  4. static:  Contains  all the static data
  5. templates HTML file that will be used for rendering
  6. utility: All the function inside the utility module will be run using the cronjob.
     The fetch_data.py file will fetch the 1m tick data from the Binance API and store in the database and other files will aggregate the 1m tick data.

   7. view: This is the view module of flask app that contains the home blueprint.
   8: model : model module handles the business logic for the home module.