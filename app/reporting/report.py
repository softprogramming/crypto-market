import os.path as path
import sys

two_up = path.abspath(path.join(__file__ ,"../../.."))
sys.path.insert(0, two_up)

from app.db_handler.report_db import  Report
from app.email_service.email_app import send_report_email


coin_map = Report("coin_map")
number_of_market = coin_map.get_market_count()

data_points_expected = number_of_market * 60 * 24

total_data_points_available = coin_map.get_total_data_count()

total_data_points_for_each_coin = coin_map.get_each_market_data()


send_report_email(number_of_market,data_points_expected,
                  total_data_points_available, total_data_points_for_each_coin)