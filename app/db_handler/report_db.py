from app.db_handler.db_wrapper  import  SimpleMysql

db = SimpleMysql()


class Report:
    """Common base class for all employees"""

    def __init__(self, table):
        self.table = table

    def get_market_count(self):
        query = "select count(*) from coin_map where 1=1;"
        result = db.query(query).fetchone()
        return result[0]

    def expected_data_points(self):
        print("Name : ", self.name, ", Salary: ", self.salary)

    def get_total_data_count(self):
        count = 0
        query = "select count(*) from tic_data_1m where 1=1;"
        count = count + db.query(query).fetchone()[0]
        return count

    def get_each_market_data(self):
        query = "select market, count(*) from tic_data_1m  group by market;"
        count = db.query(query).fetchall()
        return count
