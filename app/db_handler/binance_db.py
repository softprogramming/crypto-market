from app.db_handler.db_wrapper  import  SimpleMysql


def insert_data(table_name, market, open_price, close, high, low, volume, time_stamp):
    db = SimpleMysql()
    db.insert(table_name, {"market": market, "open": open_price, "close": close,
                              "high": high, "low":low, "volume": volume,"time_stamp":time_stamp})
    db.end()


def insert_data_in_batch(table_name, data):
    db = SimpleMysql()
    db.insertBatch(table_name, data)
    db.end()


def get_market_list():
    db = SimpleMysql()
    market = db.getAll("coin_map", ["market"])
    db.end()
    return  market


def getpair_data(coin_pair, table_name):
    db = SimpleMysql()
    market = db.getAll(table_name, ["time_stamp","open","close", "high","low", "volume"],
                       ("market = %s", [coin_pair]), ["time_stamp", "DESC"], [0, 30]	)
    db.end()
    return market


def get_last_time(coin_pair, table_name):
    db = SimpleMysql()
    time = db.getOne(table_name, ["time_stamp"], ["market = %s", [coin_pair]], ["time_stamp", "DESC"])
    db.end()
    return time


""""Code  for 5 minute data"""


def getpair_data_5m(coin_pair, last_time=None):
    db = SimpleMysql()
    if last_time is None:
        market = db.getAll("tic_data_1m", ["time_stamp","open","close", "high","low", "volume"],
                       ("market = %s", [coin_pair]), ["time_stamp", "ASC"])
        db.end()
        return market
    else:
        market = db.getAll("tic_data_1m", ["time_stamp", "open", "close", "high", "low", "volume"],
                           ("market = %s and time_stamp > %s", [coin_pair,last_time.time_stamp]), ["time_stamp", "ASC"])
        db.end()
        return  market


""""Code  for 15 minute data"""


def getpair_data_15m(coin_pair, last_time=None):
    db = SimpleMysql()
    if last_time is None:
        market = db.getAll("tic_data_1m", ["time_stamp","open","close", "high","low", "volume"],
                       ("market = %s", [coin_pair]), ["time_stamp", "ASC"])
        db.end()
        return market
    else:
        market = db.getAll("tic_data_1m", ["time_stamp", "open", "close", "high", "low", "volume"],
                           ("market = %s and time_stamp > %s", [coin_pair,last_time.time_stamp]), ["time_stamp", "ASC"])
        db.end()
        return  market


""""Code  for 30 minute data"""


def getpair_data_30m(coin_pair, last_time=None):
    db = SimpleMysql()
    if last_time is None:
        market = db.getAll("tic_data_1m", ["time_stamp","open","close", "high","low", "volume"],
                       ("market = %s", [coin_pair]), ["time_stamp", "ASC"])
        db.end()
        return market
    else:
        market = db.getAll("tic_data_1m", ["time_stamp", "open", "close", "high", "low", "volume"],
                           ("market = %s and time_stamp > %s", [coin_pair,last_time.time_stamp]), ["time_stamp", "ASC"])
        db.end()
        return  market


""""Code  for 60 minute data"""


def getpair_data_60m(coin_pair, last_time=None):
    db = SimpleMysql()
    if last_time is None:
        market = db.getAll("tic_data_1m", ["time_stamp","open","close", "high","low", "volume"],
                       ("market = %s", [coin_pair]), ["time_stamp", "ASC"])
        db.end()
        return market
    else:
        market = db.getAll("tic_data_1m", ["time_stamp", "open", "close", "high", "low", "volume"],
                           ("market = %s and time_stamp > %s", [coin_pair,last_time.time_stamp]), ["time_stamp", "ASC"])
        db.end()
        return  market
