from flask import Flask

from app.db_handler import binance_db

app = Flask(__name__)


from views.home import home

app.register_blueprint(home)