"""Home Model"""

from app.db_handler import binance_db


def klines_handler(args):
    """
    Handle the klines related request
    :param dict: request argument
    :return boolean: boolean
    :return list of data: coin data
    """
    status, messsage= validation_args(args)
    if status is False:
        return status, message

    coin_pair = args.get('symbol', None)
    interval = args.get('interval', None)
    data = get_data(coin_pair, interval)
    return True, data


def validation_args(args):
    """Validated the request data"""
    if args.get('symbol', None) is None:
        return False, "Missing coin pair parameter."

    if args.get('interval', None) is None:
        return False, "Missing interval parameter."
    return True, "Data Validated"


def get_data(coin_pair, interval):
    """
    Get the market realted data based on the interval
    :param string : coin_pair:
    :param string: interval:
    :return: list: market data
    """
    table_name = get_table_name(interval)
    data = binance_db.getpair_data(coin_pair, table_name)
    data = sorted(data, key=sort_based_on_id_timestamp)
    return data

def sort_based_on_id_timestamp(item):
    """ Return the code names """
    return item[0]


def get_table_name(interval):
    """
    Return the table name based on the  the interval
    :param  string: interval
    :return string: table name
    """
    return {
        "1m": "tic_data_1m",
        "5m": "tic_data_5m",
        "15m" :"tic_data_15m",
        "30m": "tic_data_30m",
        "30m": "tic_data_30m"
    }.get(interval, "tic_data_1m")
