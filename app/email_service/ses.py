import boto3

from app.email_service.email_conf import AWS_ACCESS_KEY, AWS_SECRET_KEY

ses = boto3.client('ses', region_name='us-east-1', aws_access_key_id=AWS_ACCESS_KEY,
                   aws_secret_access_key=AWS_SECRET_KEY)

print(AWS_ACCESS_KEY)
print(AWS_SECRET_KEY)

def send_to_ses(from_address, to_address, subject, htmlbody):
    """Pass data to amazon SES"""
    try:
        response = ses.send_email(
            Source=from_address,
            Destination={
                "ToAddresses": [
                    to_address,
                ]
            },
            Message={
                "Body": {
                    "Html": {
                        "Charset": "UTF-8",
                        "Data": htmlbody,
                    },
                    "Text": {
                        "Charset": "UTF-8",
                        "Data": "This is simplemail",
                    },
                },
                "Subject": {
                    "Charset": "UTF-8",
                    "Data": subject,
                },
            }
        )
        print(response)
        return True, response
    except Exception as e:
        print(str(e))
        return False


def send_mail(email_data):
    try:
        from_address = email_data['from_address']
        to_address = email_data['to_address']
        subject = email_data['subject']
        message = email_data['html']
        success, response = send_to_ses(from_address, to_address, subject, message)
        return response
    except Exception as e:
        return str(e)