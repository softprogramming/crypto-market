"""Email Handler"""
import os

from jinja2 import Template


from app.email_service.email_conf import TO_EMAIL
from app.email_service.ses import send_mail


EMAIL_SUBJECT = {'market_report': 'Market Report'}

REPORT_TEMPLATE = os.path.join(os.getcwd(), "report.html")
with open(REPORT_TEMPLATE, "r") as f:
    DATA = f.read()
REPORT_TEMPLATE = Template(DATA)


def send_report_email(number_of_market, data_points_expected,
                      total_data_points_available, total_data_points_for_each_market):
    """Send the report email"""
    data = REPORT_TEMPLATE.render(number_of_market=number_of_market, expected_data_points=data_points_expected,
                                  total_data_points_available=total_data_points_available,
                                  total_data_points_for_each_market=total_data_points_for_each_market)

    email_data = {'from_address': "rhlsngh302@gmail.com", 'to_address': TO_EMAIL, 'subject': EMAIL_SUBJECT['market_report'], 'html': data}
    mail_status = send_mail(email_data)
    return mail_status