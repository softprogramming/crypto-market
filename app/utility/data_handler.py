from app.db_handler import binance_db


def extract_data(table_name, coin_pair_data, row, interval, miliseconds):
    index = 1
    high = 0
    low = 0
    time_diff = 0
    for data in coin_pair_data:
        if data.high > high:
            high = data.high

        if index == 1:
            low = data.low
            first_time = data.time_stamp
        else:
            time_diff = first_time - data.time_stamp

        if data.low < low:
            low = data.low

        if index % interval == 1:
            open = data.open

        if index % interval == 0 or time_diff == miliseconds:
            close = data.close
            volume = data.volume
            index = 0
            time_diff = 0
            binance_db.insert_data(table_name, row.market, open, close, high, low, volume, first_time)
        index += 1
