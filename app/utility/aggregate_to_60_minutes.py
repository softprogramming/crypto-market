import os.path as path
import sys

two_up = path.abspath(path.join(__file__ ,"../../.."))
sys.path.insert(0, two_up)

from app.utility.data_handler import extract_data
from app.db_handler import  binance_db

market = binance_db.get_market_list()


def aggregate_data_to_60m():
    for row in market:
        try:
            last_time = binance_db.get_last_time(row.market, "tic_data_60m")
            coin_pair_data = binance_db.getpair_data_60m(row.market, last_time)
            extract_data("tic_data_60m", coin_pair_data, row, 60, -3540000)
        except Exception as e:
            print(row)
            print(str(e))



aggregate_data_to_60m()
