import os.path as path
import sys

from datetime import datetime
import ccxt

two_up =  path.abspath(path.join(__file__ ,"../../.."))
sys.path.insert(0, two_up)

from app.db_handler import binance_db

exchange = ccxt.binance()

market = binance_db.get_market_list()


def get_date_time(miliseconds=None):
    """
    Converts seconds since the Epoch to a time tuple expressing UTC.
    When 'seconds' is not passed in, convert the current time instead.
    :Parameters:
        - `seconds`: time in seconds from the epoch.
    :Return:
    Time in UTC format.
    """
    date = datetime.fromtimestamp(miliseconds / 1000)
    return date


def data_handler():
    """Main function to fetch and insert 1 minute data"""
    for row in market:
        try:
            market_data = exchange.fetch_ohlcv(row.market, "1m")
            last_time = binance_db.get_last_time(row.market, "tic_data_1m")
            data = []
            for md in market_data:
                insert_into_dict(last_time, md, data, row)
            binance_db.insert_data_in_batch("tic_data_1m", data)
        except Exception as e:
            print(str(e))


def insert_into_dict(last_time, md, data, row):
    """Insert data into dict"""
    row = {"market": row.market, "open": md[1], "close": md[2], "high": md[3], "low": md[4], "volume": md[5],
           "time_stamp": md[0]}
    if last_time is None:
        data.append(row)
    else:
        if md[0] > last_time.time_stamp:
            data.append(row)


data_handler()
