""""Blueprint  for handling the routes on home """

from flask import Blueprint, jsonify, request, render_template

from app.db_handler import binance_db
from app.model.home_handler import klines_handler

home = Blueprint('home', __name__)


@home.route('/')
def base_url():
    """Render the base html file"""
    market = binance_db.get_market_list()
    coin_pair = []
    for coin in market:
        coin_pair.append(coin.market)

    return render_template("index.html", coin_pair= coin_pair)


@home.route('/klines', methods=["GET"])
def klines():
    """
    Returns the market data
    :param: str: symbol
    :param: str: time interval in minute
    :return: json : coin pair value
    """
    print (type(request.args))
    status, data = klines_handler(request.args)
    return jsonify(status=status, data=data)


